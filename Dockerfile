FROM python:3.7

RUN mkdir /apigee_exporter

COPY static/apigee_exporter.py /apigee_exporter/apigee_exporter.py
COPY static/Pipfile /apigee_exporter/Pipfile
COPY static/logs /apigee_exporter/logs

WORKDIR /apigee_exporter

RUN pip install pipenv

RUN pipenv install --dev

CMD ["pipenv", "run", "python", "apigee_exporter.py", "8192"]