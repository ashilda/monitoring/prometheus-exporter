import sys
import json
import re
from time import sleep
from prometheus_client import start_http_server, Summary, Counter


class LogProcessor():

    def __init__(self, response_metric, error_metric):
        self.response_metric = response_metric
        self.error_metric = error_metric

    def decode_log(self, log):
        jsonlog = re.findall(r'Payload:= {"logPayload":(\{.+\})}$', log)
        log_dict = json.loads(jsonlog[0])
        return log_dict['log_type'], log_dict

    def extract_method(self, url):
        decoded = re.match(r'https?:\/\/[^\/]+(?P<base>\/[^\/]+)?(?P<version>\/v[0-9]{1}(\.[0-9]{1})?)(?P<api>\/[^\/]+)(?P<method>\/[^\/?]+(\/\w+)?(\/[^\/]+)?)', url)   
        method = self.clean_url(decoded.group('method'))
        return method

    def clean_url(self, url):
        url = re.sub(r'(?:(?<=account\/)|(?<=accounts\/)|(?<=person\/))[^\/]+','_',url)
        url = re.sub(r'\?.+','',url)
        return url

    def publish_response_metrics(self, log_dict):
        method = self.extract_method(log_dict['request']['url'])
        self.response_metric.labels(env=log_dict['env'], proxy=log_dict['proxy']['name'], verb=log_dict['request']['verb'], method=method, status_code=log_dict['response']['statusCode']).observe(float(log_dict['response']['duration']) / 1000)

    def publish_error_metrics(self, log_dict):
        self.error_metric.labels(env=log_dict['env'], proxy=log_dict['proxy']['name'], flow=log_dict['flow_name'], source=log_dict['error']['source'], error_code=log_dict['error']['error_code']).inc()

    def is_apigee_log(self,log):
        return bool(re.search(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\s+', log))

    def process_log(self, line):
        if self.is_apigee_log(line) is False:
            return
        try:
            log_type, decoded_log = self.decode_log(line)
            if log_type == 'proxy.response':
                self.publish_response_metrics(decoded_log)
            elif log_type == 'error':
                self.publish_error_metrics(decoded_log)
            else:
                return
        except:
            print(sys.exc_info())
        return


if __name__ == '__main__':

    port = int(sys.argv[1])
    start_http_server(port)

    REQUEST_SUMMARY = Summary('digitalapi_request', 'Count of requests', ['env', 'proxy', 'verb', 'method', 'status_code'])
    ERROR_COUNT = Counter('digitalapi_error', 'Count of errors - preflow or postflow', ['env', 'flow', 'proxy', 'source', 'error_code'])

    LogProcessor = LogProcessor(REQUEST_SUMMARY, ERROR_COUNT)
    f = open("logs", "r")
    f1 = f.readlines()
    arr = []
    for line in f1:
        arr.append(line)
    while True:
        for line in arr:
            sleep(0.5)
            LogProcessor.process_log(line)
    
    