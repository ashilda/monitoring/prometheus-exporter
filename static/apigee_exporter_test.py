import pytest
import requests
from apigee_exporter import LogProcessor
from prometheus_client import start_http_server, Summary, Counter


log_proxy_success = r'10.34.0.9 [25/07/2019 15:50:00.0000] Payload:= {"logPayload":{"env": "dev","logType": "proxy.response","request": {"verb": "GET","url": "https://api-digital-dev.ybs.co.uk/digital/v1/account/accounts/Y5C7DL4ERSYMAT64QZRWTMU5GQ000000/transactions?page=1"},"response": {"statusCode": "200","duration": "150"},"proxy": {"name": "test-proxy-service"}}}'
log_proxy_failure = r'10.34.0.9 [26/07/2019 11:25:21.3000] Payload:= {"logPayload":{"timestamp":"2019-07-26T10:25:21.227Z","flowName":"PostFlow","org":"ybsg-digital-nonprod","env":"dev","requestId":"5b0efacb-405b-45e0-8b1d-b74c20aaba61","registrationId":"44a5614b-a7cf-43d8-bdfd-716be9bedcc6","client":{"clientIP":"213.38.213.226","clientHost":"213.38.213.226","certCN":"","organizationName":"cert.org"},"proxy":{"name":"account-savingsapp-v1","revision":"14"},"logType":"proxy.response","request":{"headers":"User-Agent:python-requests/2.21.0,x-mock-service:true,x-request-signature:MEUCIQCYc/mtBpB7zaWSQ/v6kvDo+bmLi+3h/sQ8b06YuNVcqQIgOrfoAn9PYKBtysD4wlvHTTSp0dXcv5LBjmQm8YPu5HE=","verb":"GET","url":"https://api-digital-dev.ybs.co.uk/digital/v1/account/accounts/Y5C7DL4ERSYMAT64QZRWTMU5GQ000000","timestamp":"1564136721119","payload":""},"response":{"headers":"Content-Length:320,Content-Type:application/json","statusCode":"404","timestamp":"1564136721227","payload":"{\n \"Id\": \"5b0efacb-405b-45e0-8b1d-b74c20aaba61\",\n \"Code\": \"404 \",\n \"Message\": \"Unknown Resource\",\n \"Url\": \"https://developers.ybs.co.uk\",\n \"Errors\": [\n  {\n   \"ErrorCode\": \"UnknownResource\",\n   \"Message\": \"Resource not found.\"\n  }\n ]\n}","duration":"108"}}}'
log_error_postflow = r'10.34.0.9 [26/07/2019 11:25:21.3100] Payload:= {"logPayload":{"timestamp":"2019-07-26T10:25:21.219Z","flowName":"PostFlow","org":"ybsg-digital-nonprod","env":"dev","requestId":"5b0efacb-405b-45e0-8b1d-b74c20aaba61","registrationId":"44a5614b-a7cf-43d8-bdfd-716be9bedcc6","client":{"clientIP":"127.0.0.1","clientHost":"127.0.0.1","certCN":"","organizationName":"cert.org"},"proxy":{"name":"account-enterprise-v1","revision":"11"},"logType":"error","request":{"headers":"Content-Length:1269,Content-Type:text/xml,User-Agent:python-requests/2.21.0,x-mock-service:true,x-request-signature:MEUCIQCYc/mtBpB7zaWSQ/v6kvDo+bmLi+3h/sQ8b06YuNVcqQIgOrfoAn9PYKBtysD4wlvHTTSp0dXcv5LBjmQm8YPu5HE=","verb":"POST","url":"http://api-digital-dev.ybs.co.uk/v1/savingsapp/mock/getSavingsAccount?123456","timestamp":"1564136721176","payload":"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v11=\"http://schema.ybs.co.uk/EO/common/system/messageheader/v1\" xmlns:v2=\"http://schema.ybs.co.uk/EM/customersupport/account/business/savingsaccountbusiness/getsavingsaccount/v2\"><soapenv:Body><v2:GetSavingsAccount><v11:MessageHeader><v11:AuditInfo><v11:ConsumerSystemID>api-digital-dev.ybs.co.uk</v11:ConsumerSystemID><v11:ConsumerMessageID>5b0efacb-405b-45e0-8b1d-b74c20aaba61</v11:ConsumerMessageID><v11:Environment>5</v11:Environment><v11:RequestedDateTime>2019-07-26T10:25:21</v11:RequestedDateTime>\n                            </v11:AuditInfo><v11:MessageContext><v11:HostName>API-Dev</v11:HostName><v11:UserId>APIGUser</v11:UserId><v11:CreationDateTime>2019-07-26T10:25:21</v11:CreationDateTime><v11:BrandCodeList><v11:BrandCode>GRP</v11:BrandCode>\n                                </v11:BrandCodeList>\n                            </v11:MessageContext>\n                        </v11:MessageHeader><v2:DataArea><v2:AccountNumberList><v2:AccountNumber>1234</v2:AccountNumber>\n                            </v2:AccountNumberList>\n                        </v2:DataArea>\n                    </v2:GetSavingsAccount>\n                </soapenv:Body>\n            </soapenv:Envelope>\n        "},"response":{"headers":"Content-Length:1501,Content-Type:application/xml","statusCode":"404","timestamp":"1564136721216","payload":"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n\t<env:Body xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n\t\t<env:Fault xmlns:flt=\"http://service.ybs.co.uk/customersupport/account/proxy/SavingsAccount_Business/v2/getSavingsAccount\">\r\n\t\t\t<faultcode>flt:InvalidIdentifierFault</faultcode>\r\n\t\t\t<faultstring>Invalid Identifier Fault.</faultstring>\r\n\t\t\t<faultactor/>\r\n\t\t\t<detail>\r\n\t\t\t\t<iif3:InvalidIdentifierFault xmlns:iif3=\"http://schema.ybs.co.uk/common/fault/invalididentifierfault/v3\">\r\n\t\t\t\t\t<gnf3:Type xmlns:gnf3=\"http://schema.ybs.co.uk/common/fault/genericfault/v3\">InvalidIdentifierFault</gnf3:Type>\r\n\t\t\t\t\t<gnf3:Code xmlns:gnf3=\"http://schema.ybs.co.uk/common/fault/genericfault/v3\">E-002</gnf3:Code>\r\n\t\t\t\t\t<gnf3:Description xmlns:gnf3=\"http://schema.ybs.co.uk/common/fault/genericfault/v3\">Account Number not found</gnf3:Description>\r\n\t\t\t\t\t<gnf3:ContextId xmlns:gnf3=\"http://schema.ybs.co.uk/common/fault/genericfault/v3\">-InitialisingPipeline[request-pipeline-InvokeParty_DataStage]</gnf3:ContextId>\r\n\t\t\t\t\t<gnf3:EnvironmentId xmlns:gnf3=\"http://schema.ybs.co.uk/common/fault/genericfault/v3\">PARENT: apig-soa-nonprod.ybs.co.uk:443//svc/CustomerSupport/Account/SavingsAccount_Business/v2</gnf3:EnvironmentId>\r\n\t\t\t\t\t<gnf3:SystemId xmlns:gnf3=\"http://schema.ybs.co.uk/common/fault/genericfault/v3\">SOA</gnf3:SystemId>\r\n\t\t\t\t</iif3:InvalidIdentifierFault>\r\n\t\t\t</detail>\r\n\t\t</env:Fault>\r\n\t</env:Body>\r\n</soapenv:Envelope>\r\n","duration":"37"},"error":{"source":"SOA","policy":"null/null","errorCode":"404","errorMessage":"\n            {\n                \"Id\":\"5b0efacb-405b-45e0-8b1d-b74c20aaba61\",\n                \"Code\":\"404 \",\n                \"Message\":\"Unknown Resource\",\n                \"Url\":\"https://developers.ybs.co.uk\",\n                \"Errors\":[{\"ErrorCode\":\"UnknownResource\",\"Message\":\"Resource not found.\"}]\n            }\n        ","isCatchAll":"true","errorType":"InvalidIdentifierFault"}}}'
log_error_preflow = r'10.34.0.9 [29/07/2019 10:43:00.6840] Payload:= {"logPayload":{"timestamp":"2019-07-29T09:43:00.651Z","flowName":"PreFlow","org":"ybsg-digital-nonprod","env":"test","requestId":"02afb925-3cd8-49bd-a370-92e8a576a8fd","registrationId":"b5a27a39-09e8-4b8a-92e2-085e72abe78b","client":{"clientIP":"213.38.213.226","clientHost":"213.38.213.226","certCN":"","organizationName":"cert.org"},"proxy":{"name":"account-savingsapp-v1","revision":"14"},"logType":"error","request":{"headers":"User-Agent:python-requests/2.22.0,x-mock-service:true,x-request-signature:MEUCIQDy37Po5acP7DeR8nC+Hn9ERU4x0ChtzanqFisXCpifsAIgBI8O2vndeod2ubgxdjkbXFduRQn1faFY/AZQ5czoYKk=","verb":"GET","url":"https://api-digital-test.ybs.co.uk/digital/v1/account/accounts","timestamp":"1564393380593","payload":""},"error":{"source":"Policy","policy":"","errorCode":"412","errorMessage":"\n            {\n                \"Id\":\"02afb925-3cd8-49bd-a370-92e8a576a8fd\",\n                \"Code\":\"412 Precondition Failed\",\n                \"Message\":\"Precondition Failed.\",\n                \"Url\":\"https://developers.ybs.co.uk\",\n                \"Errors\":[ { \"ErrorCode\": \"Unsupported.AppVersion\", \"Message\": \"Minimum app version supported: 1.0\" } ]\n            }\n        ","isCatchAll":"false","errorType":"Proxy"}}}'
bad_log = r'PayloadTooLargeError: request entity too large'

@pytest.fixture(scope="module")
def log_processor():
    REQUEST_SUMMARY = Summary('digitalapi_request', 'Count of requests', ['env','proxy', 'verb', 'method', 'status_code'])
    ERROR_COUNT = Counter('digitalapi_error', 'Count of backend errors', ['env','proxy', 'flow', 'source', 'error_code'])

    start_http_server(8888)
    return LogProcessor(REQUEST_SUMMARY, ERROR_COUNT)

def test_bad_log(log_processor):
    assert log_processor.is_apigee_log(bad_log) is False

def test_good_log(log_processor):
    assert log_processor.is_apigee_log(log_proxy_success) is True

def test_extract_json_log_success_proxy_response(log_processor):
    log_type, log_dict = log_processor.decode_log(log_proxy_success)
    assert log_type == 'proxy.response'
    assert log_dict['env'] == 'dev'

def test_extract_json_log_failure_proxy_response(log_processor):
    log_type, log_dict = log_processor.decode_log(log_proxy_failure)
    assert log_type == 'proxy.response'
    assert log_dict['env'] == 'dev'

def test_extract_json_error_log_payload(log_processor):
    log_type, log_dict = log_processor.decode_log(log_error_postflow)
    assert log_type == 'error'
    assert log_dict['env'] == 'dev'

def test_extract_account_list_url(log_processor):
    url = 'https://api-digital-dev.ybs.co.uk/digital/v1/account/accounts'
    assert log_processor.extract_method(url) == '/accounts'

def test_extract_account_detail_url(log_processor):
    url = 'https://api-digital-dev.ybs.co.uk/digital/v1/account/accounts/Y5C7DL4ERSYMAT64QZRWTMU5GQ000000'
    assert log_processor.extract_method(url) == '/accounts/_'

def test_extract_account_transactions_url(log_processor):
    url = 'https://api-digital-dev.ybs.co.uk/digital/v1/account/accounts/Y5C7DL4ERSYMAT64QZRWTMU5GQ000000/transactions'
    assert log_processor.extract_method(url) == '/accounts/_/transactions'

def test_extract_account_transactions_paged_url(log_processor):
    url = 'https://api-digital-dev.ybs.co.uk/digital/v1/account/accounts/Y5C7DL4ERSYMAT64QZRWTMU5GQ000000/transactions?page=1'
    assert log_processor.extract_method(url) == '/accounts/_/transactions'

def test_party_person_url(log_processor):
    url = 'https://api-digital-dev.ybs.co.uk/digital/v1/party/person/12345678'
    assert log_processor.clean_url(url) == 'https://api-digital-dev.ybs.co.uk/digital/v1/party/person/_'

def test_start_exporter_and_test_success_log(log_processor):
    log_processor.process_log(log_proxy_success)
    response = requests.request('GET', url='http://localhost:8888/')
    assert 'digitalapi_request_count{env="dev",method="/accounts/_/transactions",proxy="test-proxy-service",status_code="200",verb="GET"} 1.0' in response.text
    assert 'digitalapi_request_sum{env="dev",method="/accounts/_/transactions",proxy="test-proxy-service",status_code="200",verb="GET"} 0.15' in response.text

def test_start_exporter_and_test_failure_log(log_processor):
    log_processor.process_log(log_proxy_failure)
    response = requests.request('GET', url='http://localhost:8888/')
    assert 'digitalapi_request_count{env="dev",method="/accounts/_",proxy="account-savingsapp-v1",status_code="404",verb="GET"} 1.0' in response.text
    assert 'digitalapi_request_sum{env="dev",method="/accounts/_",proxy="account-savingsapp-v1",status_code="404",verb="GET"} 0.108' in response.text

def test_start_exporter_and_test_error_postflow(log_processor):
    log_processor.process_log(log_error_postflow)
    response = requests.request('GET', url='http://localhost:8888/')
    assert 'digitalapi_error_total{env="dev",error_code="404",flow="PostFlow",proxy="account-enterprise-v1",source="SOA"} 1.0' in response.text

def test_start_exporter_and_test_error_preflow(log_processor):
    log_processor.process_log(log_error_preflow)
    response = requests.request('GET', url='http://localhost:8888/')
    assert 'digitalapi_error_total{env="test",error_code="412",flow="PreFlow",proxy="account-savingsapp-v1",source="Policy"} 1.0' in response.text
